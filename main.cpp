#include <string>
#include <deque>
#include <forward_list>
#include <iostream>

#include "worddatabase.h"
#include "wordcounter.h"

using namespace std;

int main()
{
    Words::WordDatabase wdb(2);

    string input = "a b c d";
    
    Words::WordCounter wordCounter;
    wordCounter.countAdjacentWordsExternalStorage(input, wdb.getNumOfAdjacentWords(), wdb);
    
    wdb.printWords();
    
    return 0;
}
