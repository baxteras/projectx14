#include <string>
#include <sstream>
#include <iostream>
#include <deque>
#include <forward_list>

#include "wordcounter.h"
#include "worddatabase.h"

namespace Words
{
    WordCounter::WordCounter()
    {
    }

    WordCounter::~WordCounter()
    {
    }
    
    void WordCounter::countAdjacentWordsExternalStorage(string& text,
                                         const int numOfWords,
                                         WordDatabase& wordCounts)
    {
        // The n_words deque represents a window of
        // n words that moves on the input.
        deque<string> n_words;

        // n_words_list will be used to sort the n_words window's
        // words and form a key from their combination, to be used by
        // word_counts.
        forward_list<string> n_words_list;

        stringstream reader;
        reader << text;

        int counter = 0;

        string word;
        while (reader >> word)
        {
            n_words.push_front(word);
            ++counter;

            if (counter == numOfWords + 1)
            {
                n_words.pop_back();
                --counter;
            }

            if (counter == numOfWords)
            {
                for (int i = 0; i < numOfWords; ++i)
                {
                    n_words_list.push_front(n_words[i]);
                }

                n_words_list.sort();

                string combined_words;
                for (int i = 0; i < numOfWords; ++i)
                {
                    combined_words += n_words_list.front();
                    n_words_list.pop_front();

                }

                if (wordCounts.keyExists(combined_words))
                {
                    wordCounts.addOneToValue(combined_words);
                }
                else
                {
                    wordCounts.initializeKey(combined_words);
                }
            }
        }
    }
}

