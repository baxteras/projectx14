#ifndef WORD_COUNTER_H
#define WORD_COUNTER_H

#include <string>
#include <unordered_map>

#include "worddatabase.h"

using namespace std;

namespace Words
{
    class WordCounter
    {
        public:
            WordCounter();
            ~WordCounter();
        
        void countAdjacentWordsExternalStorage(string& text,
                                               const int numOfWords,
                                               WordDatabase& wordCounts);
    };
}

#endif
