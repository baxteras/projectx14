
#include <memory>
#include <string>
#include <iostream>

#include "worddatabase.h"
#include "wordcounter.h"

namespace Words
{
    WordDatabase::WordDatabase(int adjacentWords)
    {
        numOfAdjacentWords = adjacentWords;
    }

    WordDatabase::~WordDatabase()
    {
    }

    int WordDatabase::getNumOfAdjacentWords()
    {
        return numOfAdjacentWords;
    }
    
    bool WordDatabase::keyExists(string k)
    {
        if (wordCounts.count(k))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    void WordDatabase::addOneToValue(string k)
    {
        ++wordCounts[k];
    }
    
    void WordDatabase::initializeKey(string k)
    {
        wordCounts[k] = 1;
    }
    
    void WordDatabase::printWords()
    {
        for (const auto &pair : wordCounts)
        {
            std::cout << pair.first << " : " << pair.second << std::endl;
        }
    }
}
