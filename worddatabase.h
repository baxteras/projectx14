#ifndef WORD_DATABASE_H
#define WORD_DATABASE_H

#include <unordered_map>
#include <string>

using namespace std;

namespace Words
{
    class WordDatabase
    {
        public:
            WordDatabase(int adjacentWords);
            ~WordDatabase();

            int getNumOfAdjacentWords();
            
            bool keyExists(string k);
            
            void addOneToValue(string k);
            
            void initializeKey(string k);
            
            void printWords();

        protected:
            int numOfAdjacentWords;

            //shared_ptr<unordered_map<string, int>> wordCounts;
            unordered_map<string, int> wordCounts;
    };
}

#endif
